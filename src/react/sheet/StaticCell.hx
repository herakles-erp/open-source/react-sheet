package react.sheet;

import css.Properties;
import react.sheet.CellData;
import react.sheet.CellReference;
import react.sheet.SheetRow;
import react.sheet._internal.CellWrapper;
import react.types.SyntheticEvent.MouseEvent;
import react.types.or.HandlerOrVoid;

private typedef Resolver<T> = (
	data:Map<CellReference, CellData>,
	row:String,
	col:Null<String>
)->T;

private typedef Props = {
	> StyleProps,

	@:optional var col:String;
	@:optional var children:ReactFragment;
	@:optional var render:Resolver<ReactFragment>;
	@:optional var onMouseDown:HandlerOrVoid<MouseEvent<DOMElement>->Void>;
	@:optional var onClick:HandlerOrVoid<MouseEvent<DOMElement>->Void>;
	@:optional var getStyles:Resolver<StyleProps>;
	@:optional var heading:Bool;
	@:optional var catchClicks:Bool;
	@:optional var dark:Bool;
	@:optional var sticky:Bool;
	@:optional var colSpan:Int;
	@:optional var rowSpan:Int;
	@:optional var height:CSSLength;
	@:optional var title:String;
}

class StaticCell extends ReactComponent<Props> {
	override function render():ReactFragment {
		if (props.render != null)
			return
				<ConnectedCell
					col={props.col}
					children={props.render}
					heading={props.heading}
					sticky={props.sticky}
					dark={props.dark}
					onMouseDown={props.onMouseDown}
					onClick={props.onClick}
					catchClicks={props.catchClicks}

					// Forward style props
					height={props.height}
					width={props.width}
					maxWidth={props.maxWidth}
					minWidth={props.minWidth}
					fontSize={props.fontSize}
					title={props.title}
					colSpan={props.colSpan}
					rowSpan={props.rowSpan}
					background={props.background}
					border={props.border}
					borderTop={props.borderTop}
					borderBottom={props.borderBottom}
					borderLeft={props.borderLeft}
					borderRight={props.borderRight}
					borderColor={props.borderColor}
					borderTopColor={props.borderTopColor}
					borderBottomColor={props.borderBottomColor}
					borderLeftColor={props.borderLeftColor}
					borderRightColor={props.borderRightColor}
					padding={props.padding}
					top={props.top}
					color={props.color}
					align={props.align}
					bold={props.bold}
					italic={props.italic}
					whiteSpace={props.whiteSpace}
				/>;

		return
			<CellWrapper
				height={props.height}
				title={props.title}
				onMouseDown={props.onMouseDown}
				onClick={props.onClick}
				getStyles={props.getStyles == null ? null : props.getStyles.bind(null, null, props.col)}
				children={props.children}
				heading={props.heading}
				sticky={props.sticky}
				dark={props.dark}
				catchClicks={props.catchClicks}

				// Forward style props
				colSpan={props.colSpan}
				rowSpan={props.rowSpan}
				background={props.background}
				border={props.border}
				borderTop={props.borderTop}
				borderBottom={props.borderBottom}
				borderLeft={props.borderLeft}
				borderRight={props.borderRight}
				borderColor={props.borderColor}
				borderTopColor={props.borderTopColor}
				borderBottomColor={props.borderBottomColor}
				borderLeftColor={props.borderLeftColor}
				borderRightColor={props.borderRightColor}
				padding={props.padding}
				top={props.top}
				color={props.color}
				align={props.align}
				bold={props.bold}
				italic={props.italic}
				whiteSpace={props.whiteSpace}
				width={props.width}
				maxWidth={props.maxWidth}
				minWidth={props.minWidth}
				fontSize={props.fontSize}
			/>;
	}
}

private typedef ConnectedProps = {
	> StyleProps,

	var children:Resolver<ReactFragment>;
	var col:Null<String>;
	@:optional var onMouseDown:HandlerOrVoid<MouseEvent<DOMElement>->Void>;
	@:optional var onClick:HandlerOrVoid<MouseEvent<DOMElement>->Void>;
	@:optional var getStyles:Resolver<StyleProps>;
	@:optional var heading:Bool;
	@:optional var sticky:Bool;
	@:optional var dark:Bool;
	@:optional var hidden:Bool;
	@:optional var catchClicks:Bool;
	@:optional var colSpan:Int;
	@:optional var rowSpan:Int;
	@:optional var height:CSSLength;
	@:optional var title:String;
}

@:context(RowContext.Context)
private class ConnectedCell extends ReactComponent<ConnectedProps> {
	override function render():ReactFragment {
		return <InnerConnectedCell {...props} context={context} />;
	}
}

private typedef InnerConnectedProps = {
	> ConnectedProps,
	var context:RowContextProps;
}

private class InnerConnectedCell extends ReactComponent<InnerConnectedProps> {
	override function render():ReactFragment {
		var children = props.children(props.context.data, props.context.row, props.col);
		var title:String = props.title.or(
			(Std.is(children, String) || Std.is(children, Float)) ? cast children : null
		);

		return
			<CellWrapper
				height={props.height}
				width={props.width}
				maxWidth={props.maxWidth}
				minWidth={props.minWidth}
				fontSize={props.fontSize}
				onMouseDown={props.onMouseDown}
				onClick={props.onClick}
				getStyles={props.getStyles == null ? null : props.getStyles.bind(props.context.data, props.context.row, props.col)}
				children={children}
				heading={props.heading}
				sticky={props.sticky}
				dark={props.dark}
				catchClicks={props.catchClicks}
				title={title}
				hidden={props.hidden}

				// Forward style props
				colSpan={props.colSpan}
				rowSpan={props.rowSpan}
				background={props.background}
				border={props.border}
				borderColor={props.borderColor}
				borderTopColor={props.borderTopColor}
				borderBottomColor={props.borderBottomColor}
				borderLeftColor={props.borderLeftColor}
				borderRightColor={props.borderRightColor}
				padding={props.padding}
				top={props.top}
				color={props.color}
				align={props.align}
				bold={props.bold}
				italic={props.italic}
				whiteSpace={props.whiteSpace}
			/>;
	}

	override function shouldComponentUpdate(nextProps:InnerConnectedProps, _):Bool {
		return nextProps.context.data != props.context.data
			|| !shallowCompare(props, nextProps);
	}

	// TODO: use a macro to optimize while making sure this stays in sync with props
	static function shallowCompare(a:InnerConnectedProps, b:InnerConnectedProps):Bool {
		var aFields = Reflect.fields(a);
		var bFields = Reflect.fields(b);

		if (aFields.length != bFields.length)
			return false;

		var ignored = ["children", "render", "onMouseDown", "onClick", "getStyles"];

		for (field in aFields) {
			if (ignored.contains(field)) continue;

			if (!Reflect.hasField(b, field) || Reflect.field(b, field) != Reflect.field(a, field))
				return false;
		}

		return true;
	}

}
