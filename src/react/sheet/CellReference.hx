package react.sheet;

abstract CellReference(String) from String to String {
	public static inline var SEPARATOR = ';';

	public var col(get, never):String;
	public var row(get, never):String;

	function get_col():String return this.split(SEPARATOR)[0];
	function get_row():String return this.split(SEPARATOR)[1];

	public static inline function make(col:String, row:String):CellReference {
		return col + SEPARATOR + row;
	}
}
