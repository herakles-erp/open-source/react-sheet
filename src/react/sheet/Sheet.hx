package react.sheet;

import js.html.TextAreaElement;
import js.html.KeyEvent.*;

import eventtypes.KeyboardEventType;
import eventtypes.MouseEventType;
import eventtypes.ViewEventType;
import react.sheet.CellData;
import react.sheet._internal.Context;
import react.sheet._internal.CSVUtils;
import react.types.event.SyntheticEvent.KeyboardEvent;
import react.types.SyntheticEvent.MouseEvent;

private typedef Props = {
	> SheetRenderingContext,

	var children:ReactFragment;
	@:optional var colgroups:ReactFragment;
	@:optional var header:ReactFragment;
	@:optional var footer:ReactFragment;

	// CSS overrides
	@:optional var tableClass:String;
	@:optional var tableWrapperClass:String;
	@:optional var tbodyClass:String;

	@:optional var readOnly:Bool;
	@:optional var selectionMode:SelectionMode;
	@:optional var valueKind:ValueKind;
	@:optional var colSortingAlgo:(ref1:String, ref2:String)->Int;
	@:optional var rowSortingAlgo:(ref1:String, ref2:String)->Int;
	@:optional var initialData:Map<CellReference, CellData>;
	@:optional var valueExtractor:ValueExtractor;
	@:optional var editableValueExtractor:EditableValueExtractor;
	@:optional var onUnitChanges:Array<DataChange>->Void;
	@:optional var onDataChange:Map<CellReference, CellData>->Void;
	@:optional var onSelectionChange:Null<Selection>->Void;
	@:optional var controller:SheetController;
}

class Sheet extends ReactComponent<Props> {
	static var defaultProps:Partial<Props> = {
		colSortingAlgo: SheetUtils.lettersComparison,
		rowSortingAlgo: SheetUtils.intComparison,
		selectionMode: FreeSelection
	};

	override function render():ReactFragment {
		return
			<SheetContextProvider
				allowOverflow={props.allowOverflow}
				transparentCells={props.transparentCells}
				initialData={props.initialData}
				onUnitChanges={props.onUnitChanges}
				onDataChange={props.onDataChange}
				onSelectionChange={props.onSelectionChange}
				valueKind={props.valueKind}
				valueExtractor={props.valueExtractor}
				editableValueExtractor={props.editableValueExtractor}
				colSortingAlgo={props.colSortingAlgo}
				rowSortingAlgo={props.rowSortingAlgo}
				// TODO: make sure changing this works and doesn't mess things up
				// readOnly={props.readOnly}
				// selectionMode={props.selectionMode}
				getSelectionMode={() -> props.selectionMode}
			>
				<SheetRoot
					children={props.children}
					readOnly={props.readOnly}
					controller={props.controller}
					selectionMode={props.selectionMode}
					colgroups={props.colgroups}
					header={props.header}
					footer={props.footer}
					tableClass={props.tableClass}
					tableWrapperClass={props.tableWrapperClass}
					tbodyClass={props.tbodyClass}
				/>
			</SheetContextProvider>;
	}
}

private typedef InnerProps = {
	> PublicProps,
	var classes:TClasses;
}

private typedef PublicProps = {
	var children:ReactFragment;
	@:optional var readOnly:Bool;
	@:optional var selectionMode:SelectionMode;
	@:optional var colgroups:ReactFragment;
	@:optional var header:ReactFragment;
	@:optional var footer:ReactFragment;
	@:optional var tableClass:String;
	@:optional var tbodyClass:String;
	@:optional var tableWrapperClass:String;
	@:optional var controller:SheetController;
}

private typedef TClasses = Classes<[
	outerWrapper,
	innerWrapper,
	tableWrapper,
	footerWrapper,
	root,
	footer,
	input
]>

@:publicProps(PublicProps)
@:wrap(Styles.withStyles(styles))
@:context(SheetContext.Context)
private class SheetRoot extends ReactComponent<InnerProps> {
	public static function styles(_):ClassesDef<TClasses> {
		return Styles.jss({
			outerWrapper: {
				overflowX: Auto,
				height: "100%",
				width: "100%"
			},
			innerWrapper: {
				display: "flex",
				position: Relative,
				flexDirection: Column,
				alignItems: Stretch,
				height: "100%",
			},
			tableWrapper: {
				position: Relative,
				overflowY: Auto,
				flex: "300px 1 1",

				"@media print": {
					overflowY: Visible
				}
			},
			footerWrapper: {
				flex: "1px 0 0",
				paddingTop: 16
			},
			root: {
				width: "100%",
				tableLayout: "fixed",
				borderCollapse: Collapse,
				userSelect: None
			},
			footer: {
				// TODO: check how well these 15px represent a cross-platform
				// scrollbar width...
				width: "calc(100% - 15px)",
				tableLayout: "fixed",
				borderCollapse: Collapse,

				"@media print": {
					width: "100%!important",
				}
			},
			input: {
				position: Absolute,
				zIndex: -1,
				width: 0,
				height: 0,
				opacity: 0
			}
		});
	}

	var _focused:Bool = false;
	var _bluring:Bool = false;
	var mainTableRef:ReactRef<DOMElement> = React.createRef();
	var footTableRef:ReactRef<DOMElement> = React.createRef();
	var inputRef:ReactRef<TextAreaElement> = React.createRef();

	override function render():ReactFragment {
		var tableClasses = classNames({
			'${props.classes.root}': true,
			'${props.tableClass}': props.tableClass != null
		});

		var tableWrapperClasses = classNames({
			'${props.classes.tableWrapper}': true,
			'${props.tableWrapperClass}': props.tableWrapperClass != null
		});

		return <div className={props.classes.outerWrapper}>
			<div className={props.classes.innerWrapper}>
				<textarea
					type="text"
					onKeyDown={keyDown}
					onKeyUp={keyUp}
					onBlur={blur}
					className={props.classes.input}
					ref={inputRef}
				/>

				<div className={tableWrapperClasses}>
					<table
						className={tableClasses}
						onClick={_ -> focus()}
						ref={mainTableRef}
					>
						{props.colgroups}

						<if {props.header != null}>
							<thead>
								{props.header}
							</thead>
						</if>

						<tbody className={props.tbodyClass}>
							{props.children}
						</tbody>
					</table>
				</div>

				<if {props.footer != null}>
					<div className={props.classes.footerWrapper}>
						<table className={props.classes.footer} ref={footTableRef}>
							{props.colgroups}

							<tbody>
								{props.footer}
							</tbody>
						</table>
					</div>
				</if>
			</div>
		</div>;
	}

	override function componentDidMount():Void {
		_focused = false;
		_bluring = false;

		Browser.document.addEventListener(KeyboardEventType.KeyUp, bodyKeyUp);
		Browser.document.addEventListener(MouseEventType.MouseUp, bodyMouseUp);
		// Browser.window.addEventListener(ViewEventType.Resize, onResize);

		context.registerFocus(focus);

		if (props.controller != null) {
			props.controller.update = context.update;
			props.controller.getContext = context.getContext;
			props.controller.applyChanges = context.applyChanges;
			props.controller.getValue = context.getValue;
			// props.controller.syncWidth = syncWidth;
			props.controller.isReady = () -> true;
		}

		// syncWidth();
	}

	override function componentWillUnmount():Void {
		_focused = false;
		_bluring = false;
		Browser.document.removeEventListener(KeyboardEventType.KeyUp, bodyKeyUp);
		Browser.document.removeEventListener(MouseEventType.MouseUp, bodyMouseUp);
		// Browser.window.removeEventListener(ViewEventType.Resize, onResize);
		context.unregisterFocus();
	}

	override function componentDidUpdate(prevProps:InnerProps, _):Void {
		var selection = context.getSelection();

		if (!prevProps.readOnly && props.readOnly && selection != null && selection.editing) {
			context.abortEditing();
		}

		if (prevProps.selectionMode != props.selectionMode) {
			if (!props.selectionMode.match(FreeSelection)) context.abortSelection();
		}

		// TODO: optimize this
		// syncWidth();
	}

	// function onResize(_):Void {
	// 	syncWidth();
	// }

	// function syncWidth():Void {
	// 	if (mainTableRef.current == null || footTableRef.current == null) return;
	// 	footTableRef.current.style.width = mainTableRef.current.clientWidth + "px";
	// }

	function bodyMouseUp(e:MouseEvent<DOMElement>):Void {
		Browser.window.setTimeout(function() {
			context.mouseUp();
		}, 1);
	}

	function focus():Void {
		_bluring = false;
		if (inputRef.current == null || _focused) return;
		_focused = true;
		inputRef.current.focus();
		mainTableRef.current.classList.remove('sheet-inactive');
	}

	function blur():Void {
		_bluring = true;
		_focused = false;

		Browser.window.setTimeout(function() {
			if (!_bluring) return;
			_bluring = false;
			mainTableRef.current.classList.add('sheet-inactive');
		}, 30);
	}

	function bodyKeyUp(e:KeyboardEvent<DOMElement>):Void {
		var selection = context.getSelection();
		if (selection == null || !selection.editing) return;
		if (e.keyCode == DOM_VK_ESCAPE) {
			context.abortEditing();
			focus();
		}
	}

	function keyDown(e:KeyboardEvent<TextAreaElement>):Void {
		var selection = context.getSelection();
		if (selection != null && selection.editing) return;

		var keyCode = e.keyCode;
		var ctrlKey = e.ctrlKey;
		var shiftKey = e.shiftKey;

		var hasSelection = selection != null;
		var isFreeSelection = props.selectionMode.match(FreeSelection);

		switch (keyCode) {
			case DOM_VK_A if (ctrlKey && isFreeSelection):
				context.selectAll();

			case DOM_VK_Z if (ctrlKey && !props.readOnly): context.undo();
			case DOM_VK_ESCAPE if (hasSelection): context.abortSelection();
			case DOM_VK_DELETE if (hasSelection && !props.readOnly): context.setSelectionValue(null);

			case DOM_VK_LEFT if (hasSelection):
				context.move(Left, shiftKey && isFreeSelection, ctrlKey);

			case DOM_VK_RIGHT if (hasSelection):
				context.move(Right, shiftKey && isFreeSelection, ctrlKey);

			case DOM_VK_UP if (hasSelection):
				context.move(Up, shiftKey && isFreeSelection, ctrlKey);

			case DOM_VK_DOWN if (hasSelection):
				context.move(Down, shiftKey && isFreeSelection, ctrlKey);

			case DOM_VK_X if (ctrlKey && props.readOnly):
				// Ignore ctrl+x when readOnly

			case DOM_VK_C | DOM_VK_X if (hasSelection && ctrlKey):
				var data = context.getSelection2D();
				if (data != null) {
					var csv = CSVUtils.encode(
						data.map(line -> line.map(
							data -> {
								var extractedData = context.extractEditableValue(data.value, data.ref);
								extractedData == null ? "" : Std.string(extractedData);
							}
						))
					);

					Browser.navigator.clipboard.writeText(csv);
					if (keyCode == DOM_VK_X) context.setSelectionValue(null);
				}

			case _:
		}
	}

	function keyUp(e:KeyboardEvent<TextAreaElement>):Void {
		var selection = context.getSelection();
		if (selection == null || selection.editing) return;

		switch (e.keyCode) {
			case DOM_VK_RETURN if (!props.readOnly):
				context.startEditing();

			case _ if (!props.readOnly):
				if (inputRef.current.value.length > 0) {
					var val = inputRef.current.value;
					inputRef.current.value = null;

					var pasting = val.length > 1 || e.keyCode == DOM_VK_CONTROL;
					var csval = CSVUtils.decode(val);

					if (CSVUtils.isCSV(csval) || (pasting && CSVUtils.hasSingleData(csval))) {
						var dest = selection.first(context.colSortingAlgo, context.rowSortingAlgo);
						if (dest != null) return context.paste(dest, csval);
					} else if (CSVUtils.hasSingleData(csval)) {
						context.startEditing(val);
					}
				}
		}
	}
}
