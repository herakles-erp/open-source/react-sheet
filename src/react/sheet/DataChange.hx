package react.sheet;

import react.sheet.CellData;

// Unit changes made to internal data
// Note: these changes should hold enough information to work both way, so that
// these changes can be reverted.
enum DataChange {
	CellCleared(ref:CellReference, prevValue:Null<CellValue>);
	ValueChanged(ref:CellReference, value:CellValue, prevValue:Null<CellValue>);
}
