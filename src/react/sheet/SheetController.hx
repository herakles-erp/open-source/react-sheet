package react.sheet;

import react.sheet.CellData;
import react.sheet._internal.Context;

typedef SheetController = {
	> SheetContextController,
	// var syncWidth:Void->Void;
}

typedef SheetContextController = {
	> SheetContextBaseController,

	var isReady:Void->Bool;
}

typedef SheetContextBaseController = {
	var update:Void->Void;
	var getContext:Void->SheetContextProps;
	var applyChanges:(changes:Array<DataChange>)->Void;
	var getValue:(ref:CellReference)->Null<CellValue>;
}

class SheetControllerHelper {
	public static function create():SheetController {
		var errMsg = 'Sheet controller is not ready. '
			+ 'Check controller.isReady() before calling controller methods.';

		return {
			isReady: () -> false,
			update: () -> throw errMsg,
			getContext: () -> throw errMsg,
			applyChanges: (_) -> throw errMsg,
			getValue: (_) -> throw errMsg,
			// syncWidth: () -> throw errMsg,
		};
	}
}
