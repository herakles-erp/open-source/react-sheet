package react.sheet;

import css.Properties;

import react.sheet._internal.CellWrapper;

private typedef Props = {
	> StyleProps,
	> SyncProps,

	@:optional var children:Empty;
	@:optional var heading:Bool;
	@:optional var sticky:Bool;
	@:optional var colSpan:Int;
	@:optional var rowSpan:Int;
	@:optional var height:CSSLength;
	@:optional var width:CSSLength;
}

class EmptyCell extends ReactComponent<Props> {
	override function render():ReactFragment {
		return <CellWrapper {...props} />;
	}
}
