package react.sheet;

typedef CellData = {
	var ref:CellReference;
	@:optional var value:CellValue;
	@:optional var disabled:Bool;
}

typedef ExtendedCellData = {
	var data:CellData;
	// var ref:CellReference;
	var status:CellStatus;
}

typedef CellValueData = {
	var raw:Null<String>;
	var value:ExtractedValue;
}

@:forward
abstract CellValue(CellValueData) from CellValueData {
	public static function make(raw:Null<String>, ?v:ExtractedValue) {
		return {
			raw: raw,
			value: v == null ? (raw == null ? Empty : Some(raw)) : v
		};
	}

	public function hasData():Bool {
		if (this == null) return false;

		return switch (this.value) {
			case Some(v) if (v != null && (cast v) != ""): true;
			case _: false;
		};
	}
}

enum abstract CellStatus(Int) from Int to Int {
	var None = 0;
	var Selected = 1;
	var Active = 2;
	var AreaSource = 4;
	var Editing = 8;

	public var selected(get, set):Bool;
	inline function get_selected():Bool return this & Selected > 0;
	inline function set_selected(v:Bool):Bool {
		this = v ? this | Selected : this & ~Selected;
		return v;
	}

	public var active(get, set):Bool;
	inline function get_active():Bool return this & Active > 0;
	inline function set_active(v:Bool):Bool {
		this = v ? this | Active : this & ~Active;
		return v;
	}

	public var areaSource(get, set):Bool;
	inline function get_areaSource():Bool return this & AreaSource > 0;
	inline function set_areaSource(v:Bool):Bool {
		this = v ? this | AreaSource : this & ~AreaSource;
		return v;
	}

	public var editing(get, set):Bool;
	inline function get_editing():Bool return this & Editing > 0;
	inline function set_editing(v:Bool):Bool {
		this = v ? this | Editing : this & ~Editing;
		return v;
	}
}

enum ExtractedValue {
	Invalid(?reason:String);
	Empty;
	Some(value:Any);
}
