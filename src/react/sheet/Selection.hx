package react.sheet;

import react.sheet._internal.ClickModifier;

@:using(react.sheet.Selection.SelectionUtils)
typedef Selection = {
	var cells:Set<CellReference>;
	@:noCompletion var _areaCells:Set<CellReference>;
	@:noCompletion var _validatedCells:Set<CellReference>;

	var activeCell:CellReference;
	var areaSource:CellReference;
	@:noCompletion @:optional var activeMouseSelection:Null<ClickModifier>;

	var editing:Bool;
	@:optional var initialValue:String;
}

@:publicFields
class SelectionUtils {
	static function isEmpty(selection:Selection):Bool {
		return (selection == null || selection.cells.length == 0);
	}

	static function hasMany(selection:Selection):Bool {
		if (selection == null || selection.cells.length == 0) return false;
		return (selection.cells.length > 1);
	}

	static function first(
		selection:Selection,
		colSortingAlgo:(ref1:String, ref2:String)->Int,
		rowSortingAlgo:(ref1:String, ref2:String)->Int
	):Null<CellReference> {
		if (selection == null || selection.cells.length == 0) return null;
		var sorted = selection.getArea(colSortingAlgo, rowSortingAlgo);
		return sorted[0][0];
	}

	static function last(
		selection:Selection,
		colSortingAlgo:(ref1:String, ref2:String)->Int,
		rowSortingAlgo:(ref1:String, ref2:String)->Int
	):Null<CellReference> {
		if (selection == null || selection.cells.length == 0) return null;
		var sorted = selection.getArea(colSortingAlgo, rowSortingAlgo);
		var lastRow = sorted[sorted.length - 1];
		return lastRow[lastRow.length - 1];
	}

	static function getArea(
		selection:Selection,
		colSortingAlgo:(ref1:String, ref2:String)->Int,
		rowSortingAlgo:(ref1:String, ref2:String)->Int
	):Array<Array<CellReference>> {
		var rows:Map<String, Map<String, CellReference>> = [];

		for (ref in selection.cells) {
			var row = ref.row;
			var col = ref.col;

			if (!rows.exists(row)) rows.set(row, [col => ref]);
			else rows.get(row).set(col, ref);
		}

		var rows = [for (k => cols in rows) {
			var cols = [for (ck => cv in cols) {ref: ck, v: cv}];
			cols.sort((a,b) -> colSortingAlgo(a.ref, b.ref));
			{ref: k, cols: cols.map(c -> c.v)};
		}];

		rows.sort((a,b) -> rowSortingAlgo(a.ref, b.ref));
		return rows.map(row -> row.cols);
	}
}
