package react.sheet._internal;

import css.Position;
import css.Properties;
import css.TextAlign;
import css.WhiteSpace;
import js.Lib.undefined;
import js.lib.Symbol;

import mui.core.styles.MuiTheme;
import react.types.SyntheticEvent.MouseEvent;
import react.types.or.HandlerOrVoid;

import react.sheet._internal.Context;

private typedef PublicProps = {
	> StyleProps,
	> SyncProps,

	@:optional var children:ReactFragment;
	@:optional var onMouseDown:HandlerOrVoid<MouseEvent<DOMElement>->Void>;
	@:optional var onMouseUp:HandlerOrVoid<MouseEvent<DOMElement>->Void>;
	@:optional var onMouseEnter:HandlerOrVoid<MouseEvent<DOMElement>->Void>;
	@:optional var onClick:HandlerOrVoid<MouseEvent<DOMElement>->Void>;
	@:optional var onDoubleClick:HandlerOrVoid<MouseEvent<DOMElement>->Void>;
	@:optional var getStyles:Void->StyleProps;
	@:optional var heading:Bool;
	@:optional var sticky:Bool;
	@:optional var hidden:Bool;
	@:optional var selected:Bool;
	@:optional var active:Bool;
	@:optional var editing:Bool;
	@:optional var dark:Bool;
	@:optional var catchClicks:Bool;
	@:optional var areaSource:Bool;
	@:optional var colSpan:Int;
	@:optional var rowSpan:Int;
	@:optional var height:CSSLength;
	@:optional var title:String;
}

private typedef Props = {
	> PublicProps,
	> SheetRenderingContext,
	var classes:TClasses;
}

private typedef TClasses = Classes<[
	root,
	hidden,
	selected,
	active,
	editing,
	areaSource,
	sticky,
	allowOverflow,
	light,
	dark
]>

typedef SyncProps = {
	@:optional var sync:Symbol;
	@:optional var syncMaster:Bool;
	@:optional var syncProps:Array<SyncProp>;
}

typedef SyncDef = {
	var slaves:Array<ReactRef<DOMElement>>;
	@:optional var master:ReactRef<DOMElement>;
	@:optional var props:Array<SyncProp>;
}

enum SyncProp {
	Attribute(attr:String);
	Width;
}

typedef StyleProps = {
	@:optional var background:String;
	@:optional var border:String;
	@:optional var borderTop:String;
	@:optional var borderBottom:String;
	@:optional var borderLeft:String;
	@:optional var borderRight:String;
	@:optional var borderColor:String;
	@:optional var borderTopColor:String;
	@:optional var borderBottomColor:String;
	@:optional var borderLeftColor:String;
	@:optional var borderRightColor:String;
	@:optional var color:String;
	@:optional var align:TextAlign;
	@:optional var bold:Bool;
	@:optional var italic:Bool;
	@:optional var padding:CSSLengthOrArray;
	@:optional var width:CSSLength;
	@:optional var minWidth:CSSLength;
	@:optional var maxWidth:CSSLength;
	@:optional var whiteSpace:WhiteSpace;
	@:optional var fontSize:CSSLength;

	// @:optional var position:Position;
	@:optional var top:CSSLength;
	// @:optional var bottom:CSSLength;
}

@:publicProps(PublicProps)
@:wrap(Styles.withStyles(styles))
@:wrap(SheetContext.withRenderingContext)
class CellWrapper extends ReactComponent<Props> {
	// Urghhh that's looking so bad.. :x
	public static function styles<
		TB:MuiBreakpoints,
		TM:MuiMixins,
		TO:MuiOverrides,
		TPA:MuiPaletteAction,
		TPC:MuiPaletteCommon,
		TPB:MuiPaletteBackground,
		TPT:MuiPaletteText,
		TPal:MuiPalette<TPA, TPC, TPB, TPT>,
		TP:MuiProps,
		TT:MuiTypography,
		TS:MuiShape,
		TTR:MuiTransitions,
		TZI:MuiZIndexes,
		TTheme:Theme<TB, TM, TO, TPA, TPC, TPB, TPT, MuiPalette<TPA, TPC, TPB, TPT>, TP, TT, TS, TTR, TZI>
	>(theme:TTheme):ClassesDef<TClasses> {
		return Styles.jss({
			root: {
				position: Relative,
				border: "1px solid",
				height: "1.5em",
				lineHeight: 1.5,
				minWidth: "2em",
				textAlign: Center,
				borderTopColor: "var(--border-top-color)",
				borderBottomColor: "var(--border-bottom-color)",
				borderLeftColor: "var(--border-left-color)",
				borderRightColor: "var(--border-right-color)",
				overflow: Hidden,
				textOverflow: Ellipsis,
				padding: '${theme.spacing(0.25)}px ${theme.spacing(0.5)}px',
				"--border-color": theme.palette.grey.A100,
				"--border-top-color": "var(--border-color)",
				"--border-bottom-color": "var(--border-color)",
				"--border-left-color": "var(--border-color)",
				"--border-right-color": "var(--border-color)",

				"&::before": {
					content: '""',
					display: "none",
					position: Absolute,
					left: 0,
					right: 0,
					top: 0,
					bottom: 0,
					backgroundColor: theme.palette.primary.main,
					opacity: 0.1
				},

				"&::after": {
					content: '""',
					display: "none",
					position: Absolute,
					left: -1,
					right: -1,
					top: -1,
					bottom: -1,
					border: "2px solid",
					borderColor: theme.palette.primary.light,
					pointerEvents: None
				},
			},
			allowOverflow: {
				overflow: Visible
			},
			sticky: {
				position: Sticky,
				top: -1,
				boxShadow: "0 1px var(--border-color)",
				border: "none",
				zIndex: 100,

				"@media print": {
					position: Static,
					boxShadow: "none",
					top: 0
				}
			},
			light: {
				background: theme.palette.background._default
			},
			dark: {
				"--border-color": theme.palette.grey.A700,
				backgroundColor: theme.palette.grey._900,
				color: theme.palette.grey._100
			},
			selected: {
				color: theme.palette.primary.main,

				"& *": {
					color: "currentColor"
				},

				"&::before": {
					display: "block"
				},

				".sheet-inactive &::before": {
					opacity: 0.07
				}
			},
			areaSource: {
				"&::after": {
					display: "block",
					borderStyle: "dotted"
				}
			},
			active: {
				"&::after": {
					display: "block",
					borderStyle: "solid"
				}
			},
			editing: {
				color: theme.palette.text.primary + "!important",

				".sheet-inactive &::before": {
					opacity: 0.1
				},

				".sheet-inactive &::after": {
					opacity: 1
				}
			},
			hidden: {
				visibility: "hidden"
			}
		});
	}

	var ref:ReactRef<DOMElement> = React.createRef();

	override function render():ReactFragment {
		var classes = classNames({
			'${props.classes.root}': true,
			'${props.classes.hidden}': props.hidden,
			'${props.classes.selected}': props.selected,
			'${props.classes.areaSource}': props.areaSource,
			'${props.classes.active}': props.active,
			'${props.classes.editing}': props.editing,
			'${props.classes.sticky}': props.sticky,
			'${props.classes.allowOverflow}': props.allowOverflow,
			'${props.classes.light}': !props.dark && !props.transparentCells,
			'${props.classes.dark}': props.dark,
		});

		var styles:Properties = props.getStyles == null ? null : {
			var s = props.getStyles();
			if (s == null) null;
			else {
				var background = s.background.or(props.background.or(undefined));
				var color = s.color.or(props.color.or(undefined));
				invertSelected();

				{
					background: background,
					"--border-color": s.borderColor.or(props.borderColor.or(undefined)),
					"--border-top-color": s.borderTopColor.or(props.borderTopColor.or(undefined)),
					"--border-bottom-color": s.borderBottomColor.or(props.borderBottomColor.or(undefined)),
					"--border-left-color": s.borderLeftColor.or(props.borderLeftColor.or(undefined)),
					"--border-right-color": s.borderRightColor.or(props.borderRightColor.or(undefined)),
					border: s.border.or(props.border.or(undefined)),
					borderTop: s.borderTop.or(props.borderTop.or(undefined)),
					borderBottom: s.borderBottom.or(props.borderBottom.or(undefined)),
					borderLeft: s.borderLeft.or(props.borderLeft.or(undefined)),
					borderRight: s.borderRight.or(props.borderRight.or(undefined)),
					padding: s.padding.or(props.padding.or(undefined)),
					top: s.top.or(props.top.or(undefined)),
					color: color,
					textAlign: s.align.or(props.align.or(undefined)),
					fontWeight: s.bold.or(props.bold.or(false)) ? "bold" : undefined,
					fontStyle: s.italic.or(props.italic.or(false)) ? "italic" : undefined,
					height: props.height.or(undefined),
					maxWidth: s.maxWidth.or(props.maxWidth.or(s.width.or(props.width.or(undefined)))),
					minWidth: s.minWidth.or(props.minWidth.or(s.width.or(props.width.or(undefined)))),
					fontSize: s.fontSize.or(props.fontSize.or(undefined)),
					width: s.width.or(props.width.or(undefined)),
					whiteSpace: s.whiteSpace.or(props.whiteSpace.or(undefined)),
				};
			}
		};

		if (styles == null) {
			var background = props.background.or(undefined);
			var color = props.color.or(undefined);
			invertSelected();

			styles = {
				background: background,
				"--border-color": props.borderColor.or(undefined),
				"--border-top-color": props.borderTopColor.or(undefined),
				"--border-bottom-color": props.borderBottomColor.or(undefined),
				"--border-left-color": props.borderLeftColor.or(undefined),
				"--border-right-color": props.borderRightColor.or(undefined),
				border: props.border.or(undefined),
				borderTop: props.borderTop.or(undefined),
				borderBottom: props.borderBottom.or(undefined),
				borderLeft: props.borderLeft.or(undefined),
				borderRight: props.borderRight.or(undefined),
				padding: props.padding.or(undefined),
				top: props.top.or(undefined),
				color: color,
				textAlign: props.align.or(undefined),
				fontWeight: props.bold.or(false) ? "bold" : undefined,
				fontStyle: props.italic.or(false) ? "italic" : undefined,
				height: props.height.or(undefined),
				maxWidth: props.maxWidth.or(props.width.or(undefined)),
				minWidth: props.minWidth.or(props.width.or(undefined)),
				fontSize: props.fontSize.or(undefined),
				width: props.width.or(undefined),
				whiteSpace: props.whiteSpace.or(undefined),
			};
		}

		var Comp:ReactType = props.heading ? 'th' : 'td';

		return
			<Comp
				className={classes}
				onMouseDown={props.onMouseDown}
				onMouseUp={props.onMouseUp}
				onMouseEnter={props.onMouseEnter}
				onDoubleClick={props.onDoubleClick}
				onClick={onClick}
				colSpan={props.colSpan}
				rowSpan={props.rowSpan}
				ref={props.sync != null ? ref : null}
				title={props.title}
				style={styles}
			>
				{props.children}
			</Comp>;
	}

	// Native haxe map doesn't work with js symbols?
	static var sync:js.lib.Map<Symbol, SyncDef> = new js.lib.Map();

	override function componentDidMount():Void {
		if (props.sync != null) {
			var isNew = false;
			if (!sync.has(props.sync)) {
				isNew = true;
				sync.set(props.sync, {slaves: []});
			}

			var syncDef = sync.get(props.sync);
			if (props.syncMaster) {
				syncDef.master = ref;
				syncDef.props = props.syncProps;
				if (!isNew) syncSlaves(syncDef);
			} else {
				syncDef.slaves.push(ref);
				if (!isNew) syncSlave(ref, syncDef);
			}
		}
	}

	override function componentWillUnmount():Void {
		if (props.sync != null && sync.has(props.sync)) {
			var syncDef = sync.get(props.sync);
			if (props.syncMaster) syncDef.master = null;
			else syncDef.slaves.remove(ref);
		}
	}

	override function componentDidUpdate(_, _):Void {
		if (ref.current != null && props.sync != null) {
			var syncDef = sync.get(props.sync);
			if (syncDef != null && syncDef.master != null && syncDef.slaves.length > 0 && syncDef.props != null && syncDef.props.length > 0) {
				if (props.syncMaster) syncSlaves(syncDef);
				else syncSlave(ref, syncDef);
			}
		}
	}

	function onClick(e:MouseEvent<DOMElement>):Void {
		if (props.catchClicks) {
			e.preventDefault();
			e.stopPropagation();
		}

		if (props.onClick != null) props.onClick(e);
	}

	static function syncSlaves(syncDef:SyncDef):Void {
		for (slave in syncDef.slaves) syncSlave(slave, syncDef);
	}

	static function syncSlave(slave:ReactRef<DOMElement>, syncDef:SyncDef):Void {
		if (slave.current == null) return;
		if (syncDef.props == null) return;

		for (p in syncDef.props) {
			switch (p) {
				case Attribute(attr):
					slave.current.setAttribute(attr, syncDef.master.current.getAttribute(attr));

				case Width:
					slave.current.style.width = syncDef.master.current.clientWidth + 'px';
			}
		}
	}

	// See CellWrapper.macro.hx
	static macro function invertSelected() {}
}
