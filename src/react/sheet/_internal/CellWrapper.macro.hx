package react.sheet._internal;

class CellWrapper {
	static macro function invertSelected() {
		return macro @:mergeBlock {
			if (props.selected && background != null && !ColorHelpers.isWhite(background)) {
				var c = background;

				try {
					c = ColorHelpers.brightenHex(background, -0.25);
					if (c == null) c = background;
				} catch (_) {}

				background = color.or(background == null || background == "transparent" ? undefined : "#fafafa");
				color = c;
			}
		};
	}
}
