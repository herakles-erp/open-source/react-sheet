package react.sheet._internal;

import haxe.ds.StringMap;
import js.lib.Symbol;

import react.ReactContext;
import react.sheet.CellData;
import react.sheet.SheetController;

// TODO: move to a "public" package
enum ValueKind {
	IntValues;
	FloatValues;
	// BoolValues;
	StringValues;
}

enum MoveDirection {
	Left;
	Right;
	Up;
	Down;
}

enum SelectionMode {
	NoSelection;
	SingleSelection;
	FreeSelection;
}

typedef SheetContextProps = {
	// > SheetContextData,
	> SheetContextBaseController,
	> SheetRenderingContext,

	// Public API
	var data:Map<CellReference, CellData>;
	var getData:CellReference->Null<ExtendedCellData>;
	var extractEditableValue:EditableValueExtractor;
	var setSelectionValue:Null<String>->Void;
	var move:(dir:MoveDirection, shift:Bool, ctrl:Bool)->Void;
	var paste:(ref:CellReference, values:Array<Array<String>>)->Void;
	var undo:Void->Void;
	var startEditing:(?value:String)->Void;
	var abortEditing:Void->Void;
	var getSelection:Void->Null<Selection>;
	var getSelection2D:Void->Null<Array<Array<CellData>>>;
	var abortSelection:Void->Void;

	var cellMouseDown:(ref:CellReference, mod:ClickModifier)->Void;
	var cellMouseEnter:(ref:CellReference)->Void;
	var mouseUp:Void->Void;

	// Internal API
	@:noCompletion var selectAll:Void->Void;
	@:noCompletion var focus:Void->Void;
	@:noCompletion var registerFocus:(cb:Void->Void)->Void;
	@:noCompletion var unregisterFocus:Void->Void;
	@:noCompletion var registerRow:String->Void;
	@:noCompletion var unregisterRow:String->Void;
	@:noCompletion var registerCell:(
		row:String,
		col:String,
		?data:Null<CellData>,
		?valueExtractor:ValueExtractor,
		?editableValueExtractor:EditableValueExtractor,
		?cellUpdater:ExtendedCellData->Void
	)->Void;
	@:noCompletion var unregisterCell:(row:String, col:String)->Void;
	@:noCompletion var registerSelectionListener:(ref:String, cb:SelectionListener)->Void;
	@:noCompletion var unregisterSelectionListener:(ref:String)->Void;
	@:noCompletion var colSortingAlgo:(ref1:String, ref2:String)->Int;
	@:noCompletion var rowSortingAlgo:(ref1:String, ref2:String)->Int;
}

typedef SheetRenderingContext = {
	@:optional var transparentCells:Bool;
	@:optional var allowOverflow:Bool;
}

typedef SheetContextData = {
	var data:Map<CellReference, CellData>;
	var extendedData:Map<CellReference, ExtendedCellData>;
	var selection:Null<Selection>;
}

typedef ValueExtractor = (value:String, ref:CellReference)->CellValue;
typedef EditableValueExtractor = (value:CellValue, ref:CellReference)->String;

@:callable
abstract SelectionListener((selection:Selection, prevSelection:Selection)->Void)
from (selection:Selection, prevSelection:Selection)->Void
to (selection:Selection, prevSelection:Selection)->Void {
	@:from
	static function fromSingleArg(cb:Selection->Void):SelectionListener {
		return (selection:Selection, _:Selection) -> cb(selection);
	}
}

typedef SheetContextProviderProps = {
	var value:SheetContextProps;
}

typedef SheetContextConsumerProps = {
	var children:SheetContextProps->ReactFragment;
}

class SheetContext {
	public static var Context(get, null):ReactContext<SheetContextProps>;
	public static var Provider(get, null):ReactTypeOf<SheetContextProviderProps>;
	public static var Consumer(get, null):ReactTypeOf<SheetContextConsumerProps>;

	static function get_Context() {ensureReady(); return Context;}
	static function get_Provider() {ensureReady(); return Provider;}
	static function get_Consumer() {ensureReady(); return Consumer;}

	static function ensureReady() @:bypassAccessor {
		if (Context == null) {
			Context = React.createContext();
			Context.displayName = "SheetContext";
			Consumer = Context.Consumer;
			Provider = Context.Provider;
		}
	}

	// TODO: move to some "public" place
	public static function withRenderingContext<TProps:SheetRenderingContext>(
		Comp:ReactTypeOf<TProps>
	):ReactType {
		return function(props:TProps) {
			return jsx(<Consumer>{value ->
				<Comp
					{...props}
					transparentCells={value.transparentCells}
					allowOverflow={value.allowOverflow}
				/>
			}</Consumer>);
		}
	}

	// TODO: move to some "public" place
	public static function withSelectionListener<TProps:{
		registerSelectionListener:(ref:String, cb:SelectionListener)->Void,
		unregisterSelectionListener:(ref:String)->Void
	}>(
		Comp:ReactTypeOf<TProps>
	):ReactType {
		return function(props:TProps) {
			return jsx(<Consumer>{value ->
				<Comp
					{...props}
					registerSelectionListener={value.registerSelectionListener}
					unregisterSelectionListener={value.unregisterSelectionListener}
				/>
			}</Consumer>);
		}
	}
}

private typedef Props = {
	var children:ReactFragment;
	var colSortingAlgo:(ref1:String, ref2:String)->Int;
	var rowSortingAlgo:(ref1:String, ref2:String)->Int;

	@:optional var allowOverflow:Bool;
	@:optional var transparentCells:Bool;
	@:optional var valueKind:ValueKind;
	@:optional var initialData:Map<CellReference, CellData>;
	@:optional var onDataChange:Map<CellReference, CellData>->Void;
	@:optional var onSelectionChange:Null<Selection>->Void;
	@:optional var onUnitChanges:Array<DataChange>->Void;
	@:optional var getSelectionMode:Void->SelectionMode;
	@:optional var valueExtractor:ValueExtractor;
	@:optional var editableValueExtractor:EditableValueExtractor;
}

private typedef State = {
	> SheetContextData,
	var _:Symbol;
}

class SheetContextProvider extends ReactComponent<Props, State> {
	var _rows:Set<String>;
	var _cols:Set<String>;
	var _cells:Set<CellReference>;

	var _onFocus:Null<Void->Void>;
	var _history:Array<Array<DataChange>>;
	var _cellUpdaters:Map<CellReference, ExtendedCellData->Void>;
	var _selectionListeners:Map<String, SelectionListener>;

	var defaultValueExtractor:ValueExtractor;
	var valueExtractors:StringMap<ValueExtractor>;
	var defaultEditableValueExtractor:EditableValueExtractor;
	var editableValueExtractors:StringMap<EditableValueExtractor>;

	static var defaultProps:Partial<Props> = {
		colSortingAlgo: SheetUtils.lettersComparison,
		rowSortingAlgo: SheetUtils.intComparison
	};

	public function new(props:Props) {
		super(props);

		var data = props.initialData.or([]);
		_rows = [];
		_cols = [];
		_cells = [];
		_onFocus = null;
		_cellUpdaters = [];
		_selectionListeners = [];
		_history = [];

		valueExtractors = new StringMap();
		defaultValueExtractor = SheetUtils.stringValueExtractor;
		if (props.valueExtractor != null) defaultValueExtractor = props.valueExtractor;
		else if (props.valueKind != null)
			defaultValueExtractor = switch (props.valueKind) {
				case IntValues: SheetUtils.intValueExtractor;
				case FloatValues: SheetUtils.floatValueExtractor;
				// case BoolValues: SheetUtils.boolValueExtractor;
				case StringValues: SheetUtils.stringValueExtractor;
			};

		editableValueExtractors = new StringMap();
		if (props.editableValueExtractor != null) defaultEditableValueExtractor = props.editableValueExtractor;
		else defaultEditableValueExtractor = (v:CellValue, _) -> v == null ? "" : v.raw;

		state = {
			_: new Symbol(),
			data: data,
			selection: null,
			extendedData: deriveData([], data, null),
		};
	}

	override function shouldComponentUpdate(nextProps:Props, nextState:State):Bool {
		if (state.selection != nextState.selection) {
			for (ref => data in nextState.extendedData) {
				var prevData = state.extendedData.get(ref);
				if (data == null && prevData == null) continue;

				if (data == null || prevData == null || data.status != prevData.status) {
					var update = _cellUpdaters.get(ref);
					if (update == null) trace('Cannot update cell $ref');
					else update(data);
				}
			}

			for (cb in _selectionListeners) cb(nextState.selection, state.selection);
		}

		if (state._ != nextState._) return true;
		if (state.data != nextState.data) return true;
		return ReactUtil.shallowCompare(nextProps, props);
	}

	override public function render():ReactFragment {
		var context = getContext();
		return <SheetContext.Provider value={context}>{props.children}</SheetContext.Provider>;
	}

	function update():Void {
		setState({_: new Symbol()});
	}

	// Only update context reference if data changed, to avoid extra render calls
	var lastContext:Null<SheetContextProps> = null;
	var lastContextFor:Null<Map<CellReference, CellData>> = null;

	function getContext():SheetContextProps {
		var contextChanged = state.data != lastContextFor;
		if (lastContext != null && !contextChanged) return lastContext;

		lastContextFor = state.data;
		lastContext = {
			// State
			data: state.data,
			// extendedData: state.extendedData,
			// selection: state.selection,

			// Public API
			getData: getData,
			extractEditableValue: extractEditableValue,
			setSelectionValue: setSelectionValue,
			paste: paste,
			undo: undo,
			move: move,
			cellMouseDown: cellMouseDown,
			cellMouseEnter: cellMouseEnter,
			mouseUp: mouseUp,
			startEditing: setEditing.bind(true, _),
			abortEditing: setEditing.bind(false),
			getSelection2D: getSelection,
			getSelection: () -> state.selection,
			abortSelection: abortSelection,

			// Controller API
			update: update,
			getContext: getContext,
			applyChanges: alterData,
			getValue: getValue,

			// Internal API
			selectAll: selectAll,
			focus: focus,
			registerFocus: registerFocus,
			unregisterFocus: unregisterFocus,
			registerRow: registerRow,
			unregisterRow: unregisterRow,
			registerCell: registerCell,
			unregisterCell: unregisterCell,
			registerSelectionListener: registerSelectionListener,
			unregisterSelectionListener: unregisterSelectionListener,
			colSortingAlgo: props.colSortingAlgo,
			rowSortingAlgo: props.rowSortingAlgo,

			// Rendering options
			transparentCells: props.transparentCells,
			allowOverflow: props.allowOverflow,
		};

		return lastContext;
	}

	function extractValue(value:Null<String>, ref:CellReference):CellValue {
		if (valueExtractors.exists(ref))
			return valueExtractors.get(ref)(value, ref);

		return defaultValueExtractor(value, ref);
	}

	function extractEditableValue(value:CellValue, ref:CellReference):String {
		if (editableValueExtractors.exists(ref))
			return editableValueExtractors.get(ref)(value, ref);

		return defaultEditableValueExtractor(value, ref);
	}

	function getData(ref:CellReference):Null<ExtendedCellData> {
		return state.extendedData.get(ref);
	}

	function paste(dest:CellReference, values:Array<Array<String>>):Void {
		var nbRows = values.length;
		var nbCols = 0;
		for (r in values) if (r.length > nbCols) nbCols = r.length;

		var rowIndex = _rows.indexOf(dest.row);
		if (rowIndex + nbRows > _rows.length) return trace('Error: not enough rows');

		var colIndex = _cols.indexOf(dest.col);
		if (colIndex + nbCols > _cols.length) return trace('Error: not enough columns');

		var changes = [];
		var cells = [];
		var data = state.data.copy();
		var active = dest;
		var source = dest;

		if (!CSVUtils.hasSingleData(values) || state.selection.isEmpty()) {
			for (i => row in values) {
				var rowRef = _rows[i + rowIndex];

				for (j => value in row) {
					var colRef = _cols[j + colIndex];
					var c = CellReference.make(colRef, rowRef);
					var d = data.get(c).or({ref: c});
					var oldValue = d.value;
					var newValue = extractValue(value, c);
					var change = identifyChange(c, oldValue, newValue);
					if (change != null) changes.push(change);
					data.set(c, d.copy({value: newValue}));
					cells.push(c);
				}
			}
		} else {
			var val = CSVUtils.getSingleData(values);
			var newValue = CellValue.make(val);
			active = state.selection.activeCell;

			for (c in state.selection.cells) {
				var d = data.get(c).or({ref: c});
				var change = identifyChange(c, d.value, newValue);
				if (change != null) changes.push(change);
				data.set(c, d.copy({value: newValue}));
				cells.push(c);
			}
		}

		var selection:Selection = {
			activeCell: active,
			areaSource: source,
			_areaCells: cells,
			_validatedCells: [],
			cells: cells.copy(),
			editing: false
		};

		setState({
			data: data,
			selection: selection,
			extendedData: deriveData(state.extendedData, data, selection)
		}, function() {
			if (_onFocus != null) _onFocus();
			dataChanged(changes);
			selectionChanged(selection);
		});
	}

	// TODO: store redo history
	function undo():Void {
		if (_history.length == 0) return;

		var changes = _history.pop();
		// var data = state.data.copy();
		var revertedChanges = invertChanges(changes);
		var result = applyChanges(state.data.copy(), revertedChanges);
		var changedCells = result.changedCells;
		var data = result.data;
		// var changedCells:Set<CellReference> = [];

		// for (c in changes) {
		// 	switch (c) {
		// 		case CellCleared(ref, prevValue):
		// 			changedCells.push(ref);
		// 			revertedChanges.push(identifyChange(ref, null, prevValue));
		// 			data.set(ref, data.get(ref).or({}).copy({value: prevValue}));

		// 		case ValueChanged(ref, value, prevValue):
		// 			changedCells.push(ref);
		// 			revertedChanges.push(identifyChange(ref, value, prevValue));
		// 			data.set(ref, data.get(ref).or({}).copy({value: prevValue}));
		// 	}
		// }

		var selection:Selection = {
			editing: false,
			cells: changedCells,
			_validatedCells: [],
			_areaCells: changedCells.copy(),
			activeCell: null,
			areaSource: null,
		};

		selection.activeCell = selection.last(props.colSortingAlgo, props.rowSortingAlgo);
		selection.areaSource = selection.first(props.colSortingAlgo, props.rowSortingAlgo);

		setState({
			data: data,
			selection: selection,
			extendedData: deriveData(state.extendedData, data, selection)
		}, function() {
			dataChanged(revertedChanges, true);
			selectionChanged(selection);
		});
	}

	function getValue(ref:CellReference):Null<CellValue> {
		var data = getData(ref);
		if (data == null || data.data == null) return null;
		return data.data.value;
	}

	function alterData(changes:Array<DataChange>):Void {
		var result = applyChanges(state.data.copy(), changes);

		setState(state -> {
			data: result.data,
			extendedData: deriveData(state.extendedData, result.data, state.selection)
		}, function() {
			dataChanged(changes);
		});
	}

	function applyChanges(
		data:Map<CellReference, CellData>,
		changes:Array<DataChange>
	):{changedCells:Set<CellReference>, data:Map<CellReference, CellData>} {
		var changedCells:Set<CellReference> = [];

		for (c in changes) {
			switch (c) {
				case CellCleared(ref, _):
					changedCells.push(ref);
					data.set(ref, data.get(ref).or({ref: ref}).copy({value: {value: Empty}}));

				case ValueChanged(ref, value, _):
					changedCells.push(ref);
					data.set(ref, data.get(ref).or({ref: ref}).copy({value: value}));
			}
		}

		return {
			data: data,
			changedCells: changedCells
		};
	}

	function invertChanges(changes:Array<DataChange>):Array<DataChange> {
		var ret = [for (c in changes)
			switch (c) {
				case CellCleared(ref, prevValue):
					identifyChange(ref, null, prevValue);

				case ValueChanged(ref, value, prevValue):
					identifyChange(ref, value, prevValue);
			}
		];
		ret.reverse();
		return ret;
	}

	function identifyChange(
		ref:CellReference,
		prevValue:Null<CellValue>,
		newValue:Null<CellValue>
	):Null<DataChange> {
		if (prevValue == null && newValue == null) return null;

		return switch (newValue) {
			case null | {value: Empty}:
				switch (prevValue) {
					case null | {value: Empty}: null;
					case _: CellCleared(ref, prevValue);
				};

			case {value: Invalid(reason)}:
				switch (prevValue) {
					case null: ValueChanged(ref, newValue, null);
					case {value: Invalid(r)} if (r == reason): null;
					case _: ValueChanged(ref, newValue, prevValue);
				};

			case {value: Some(value)}:
				switch (prevValue) {
					case null: ValueChanged(ref, newValue, null);
					case {value: Some(v)} if (v == value): null;
					case _: ValueChanged(ref, newValue, prevValue);
				};
		};
	}

	function dataChanged(changes:Array<DataChange>, ?skipHistory:Bool):Void {
		if (props.onDataChange != null) props.onDataChange(state.data);
		if (props.onUnitChanges != null && changes != null && changes.length > 0)
			props.onUnitChanges(changes);

		if (!skipHistory && changes != null) _history.push(changes);
	}

	// TODO: avoid firing twice?
	function selectionChanged(selection:Null<Selection>):Void {
		if (props.onSelectionChange != null) props.onSelectionChange(selection);
	}

	function getSelection():Null<Array<Array<CellData>>> {
		if (state.selection == null) return null;

		return state.selection.getArea(
			props.colSortingAlgo,
			props.rowSortingAlgo
		).map(line -> line.map(ref -> state.data.get(ref)));
	}

	function abortSelection():Void {
		var dest = state.selection == null ? null : state.selection.activeCell;

		var selection:Selection = {
			activeCell: dest,
			areaSource: dest,
			_areaCells: [dest],
			_validatedCells: [],
			cells: [dest],
			editing: false
		};

		setState({
			selection: selection,
			extendedData: deriveData(state.extendedData, state.data, selection)
		}, function() {
			if (_onFocus != null) _onFocus();
			selectionChanged(selection);
		});
	}

	function selectAll():Void {
		if (_cols.length == 0 || _rows.length == 0) return;

		var topLeft = CellReference.make(_cols[0], _rows[0]);
		var cells = [for (row in _rows) for (col in _cols) CellReference.make(col, row)];

		var selection:Selection = {
			activeCell: topLeft,
			areaSource: topLeft,
			_areaCells: cells,
			_validatedCells: [],
			cells: cells.copy(),
			editing: false
		};

		setState({
			selection: selection,
			extendedData: deriveData(state.extendedData, state.data, selection)
		}, function() {
			if (_onFocus != null) _onFocus();
			selectionChanged(selection);
		});
	}

	function setSelectionValue(value:Null<String>):Void {
		if (state.selection == null) return;

		var changes = [];
		var data = state.data.copy();
		for (c in state.selection.cells) {
			var d = data.get(c).or({ref: c});
			var oldValue = d.value;
			var newValue = extractValue(value, c);
			var d = data.get(c).or({ref: c}).copy({value: extractValue(value, c)});
			var change = identifyChange(c, oldValue, newValue);
			if (change != null) changes.push(change);
			data.set(c, d.copy({value: newValue}));
		}

		var selection:Selection = {
			editing: false,
			cells: [state.selection.activeCell],
			_validatedCells: [state.selection.activeCell],
			_areaCells: [state.selection.activeCell],
			activeCell: state.selection.activeCell,
			areaSource: state.selection.activeCell
		};

		setState({
			data: data,
			selection: selection,
			extendedData: deriveData(state.extendedData, data, selection)
		}, function() {
			if (_onFocus != null) _onFocus();
			dataChanged(changes);
			selectionChanged(selection);
		});
	}

	function setEditing(editing:Bool, ?value:String):Void {
		if (state.selection == null) return;
		if (state.selection.editing == editing) return;

		setState(function(state) {
			var selection = state.selection.copy({editing: editing, initialValue: value});

			return {
				selection: selection,
				extendedData: deriveData(state.extendedData, state.data, selection)
			};
		});
	}

	function registerRow(row:String):Void {
		_rows.add(row);
		_rows.sort(props.rowSortingAlgo);
	}

	function unregisterRow(row:String):Void {
		_rows.remove(row);
	}

	function focus():Void {
		if (_onFocus != null) _onFocus();
	}

	function registerFocus(focus:Void->Void):Void {
		_onFocus = focus;
	}

	function unregisterFocus():Void {
		_onFocus = null;
	}

	function registerCell(
		row:String,
		col:String,
		?data:Null<CellData>,
		?valueExtractor:ValueExtractor,
		?editableValueExtractor:EditableValueExtractor,
		?cellUpdater:ExtendedCellData->Void
	):Void {
		var ref = CellReference.make(col, row);
		_cells.add(ref);
		if (data != null) data.ref = ref;

		if (
			data != null
			|| !state.data.exists(ref)
			|| state.data.get(ref).value == null
			|| state.data.get(ref).value.value.match(Empty)
		) {
			data = data.or({ref: ref});
			state.data.set(ref, data);
			state.extendedData.set(ref, derive(ref, data, state.selection));
		}

		if (!_cols.has(col)) {
			_cols.push(col);
			_cols.sort(props.colSortingAlgo);
		}

		if (valueExtractor != null) valueExtractors.set(ref, valueExtractor);
		if (editableValueExtractor != null) editableValueExtractors.set(ref, editableValueExtractor);
		if (cellUpdater != null) _cellUpdaters.set(ref, cellUpdater);
	}

	function unregisterCell(row:String, col:String):Void {
		var ref = CellReference.make(col, row);
		_cells.remove(ref);
		_cellUpdaters.remove(ref);
		valueExtractors.remove(ref);

		for (c in _cells) if (c.col == col) return;
		_cols.remove(col);
	}

	function registerSelectionListener(key:String, cb:SelectionListener):Void {
		_selectionListeners.set(key, cb);
	}

	function unregisterSelectionListener(key:String):Void {
		_selectionListeners.remove(key);
	}

	function move(dir:MoveDirection, shift:Bool, ctrl:Bool):Void {
		if (state.selection == null) return;
		// js.Browser.console.log('SheetContext.move: start');
		// js.Browser.console.time('SheetContext.move');
		var cur = state.selection.activeCell;
		var isEmpty = !hasData(state.data, cur);

		var ref = switch (dir) {
			case Left:
				var i = _cols.indexOf(cur.col);
				if (i <= 0) return;

				var col = _cols.get(i - 1);
				var ref = CellReference.make(col, cur.row);

				if (ctrl) {
					if (isEmpty || !hasData(state.data, ref)) {
						while (i > 0) {
							var col = _cols.get(i - 1);
							ref = CellReference.make(col, cur.row);
							if (hasData(state.data, ref)) break;
							i--;
						}
					} else {
						while (i > 0) {
							var col = _cols.get(i - 1);
							var tmp = CellReference.make(col, cur.row);
							if (!hasData(state.data, tmp)) break;
							ref = tmp;
							i--;
						}
					}
				}

				ref;

			case Right:
				var i = _cols.indexOf(cur.col);
				if (i >= _cols.length + 1) return;

				var col = _cols.get(i + 1);
				var ref = CellReference.make(col, cur.row);

				if (ctrl) {
					var last = _cols.length - 1;

					if (isEmpty || !hasData(state.data, ref)) {
						while (i < last) {
							var col = _cols.get(i + 1);
							ref = CellReference.make(col, cur.row);
							if (hasData(state.data, ref)) break;
							i++;
						}
					} else {
						while (i < last) {
							var col = _cols.get(i + 1);
							var tmp = CellReference.make(col, cur.row);
							if (!hasData(state.data, tmp)) break;
							ref = tmp;
							i++;
						}
					}
				}

				ref;

			case Up:
				var i = _rows.indexOf(cur.row);
				if (i <= 0) return;

				var row = _rows.get(i - 1);
				var ref = CellReference.make(cur.col, row);

				if (ctrl) {
					if (isEmpty || !hasData(state.data, ref)) {
						while (i > 0) {
							var row = _rows.get(i - 1);
							ref = CellReference.make(cur.col, row);
							if (hasData(state.data, ref)) break;
							i--;
						}
					} else {
						while (i > 0) {
							var row = _rows.get(i - 1);
							var tmp = CellReference.make(cur.col, row);
							if (!hasData(state.data, tmp)) break;
							ref = tmp;
							i--;
						}
					}
				}

				ref;

			case Down:
				var i = _rows.indexOf(cur.row);
				if (i >= _rows.length + 1) return;

				var row = _rows.get(i + 1);
				var ref = CellReference.make(cur.col, row);

				if (ctrl) {
					var last = _rows.length - 1;

					if (isEmpty || !hasData(state.data, ref)) {
						while (i < last) {
							var row = _rows.get(i + 1);
							ref = CellReference.make(cur.col, row);
							if (hasData(state.data, ref)) break;
							i++;
						}
					} else {
						while (i < last) {
							var row = _rows.get(i + 1);
							var tmp = CellReference.make(cur.col, row);
							if (!hasData(state.data, tmp)) break;
							ref = tmp;
							i++;
						}
					}
				}

				ref;
		};

		if (!_cells.has(ref)) return;

		if (shift) {
			var cells = getRange(state.selection.areaSource, ref);
			if (cells == null) return;

			var selection:Selection = {
				cells: state.selection._validatedCells.concat(cells),
				_areaCells: cells,
				_validatedCells: state.selection._validatedCells,
				activeCell: ref,
				areaSource: state.selection.areaSource,
				editing: false
			};

			setState({
				selection: selection,
				extendedData: deriveData(state.extendedData, state.data, selection)
			}, function() {
				selectionChanged(selection);
			});
		} else {
			var selection:Selection = {
				cells: [ref],
				_areaCells: [ref],
				_validatedCells: [],
				activeCell: ref,
				areaSource: ref,
				editing: false
			};

			setState({
				selection: selection,
				extendedData: deriveData(state.extendedData, state.data, selection)
			}, function() {
				selectionChanged(selection);
			});
		}

		// js.Browser.console.log('SheetContext.move: setState');
		// js.Browser.console.time('SheetContext.move.setState');
		// setState({});
		// js.Browser.console.timeEnd('SheetContext.move.setState');
		// js.Browser.console.time('SheetContext.move.setState');
		// setState({});
		// js.Browser.console.timeEnd('SheetContext.move.setState');

		// js.Browser.console.timeEnd('SheetContext.move');
		// js.Browser.console.log('SheetContext.move: end');
	}

	function cellMouseDown(ref:CellReference, mod:ClickModifier):Void {
		var selection:Selection = state.selection;
		var isFreeSelection = props.getSelectionMode().match(FreeSelection);

		switch (mod) {
			case None:
				selection = {
					cells: [ref],
					_areaCells: [ref],
					_validatedCells: [],
					activeCell: ref,
					areaSource: ref,
					activeMouseSelection: mod,
					editing: false
				};

			case Shift | Ctrl(_, _) if (selection == null || !isFreeSelection):
				selection = {
					cells: [ref],
					_areaCells: [ref],
					_validatedCells: [],
					activeCell: ref,
					areaSource: ref,
					activeMouseSelection: mod,
					editing: false
				};

			case Ctrl(_, _):
				var areaCells = [ref];
				var validatedCells = selection._validatedCells.concat(selection._areaCells);

				selection = {
					cells: validatedCells.concat(areaCells),
					_areaCells: areaCells,
					_validatedCells: validatedCells,
					activeCell: ref,
					areaSource: ref,
					activeMouseSelection: Ctrl(selection.activeCell, selection.areaSource),
					editing: false
				};

			case Shift:
				var cells = getRange(selection.areaSource, ref);
				if (cells == null) return;

				selection = {
					cells: selection._validatedCells.concat(cells),
					_validatedCells: selection._validatedCells,
					_areaCells: cells,
					activeCell: ref,
					areaSource: selection.areaSource,
					activeMouseSelection: Shift,
					editing: false
				};
		}

		setState({
			data: state.data,
			selection: selection,
			extendedData: deriveData(state.extendedData, state.data, selection)
		}, function() {
			selectionChanged(selection);
		});
	}

	function cellMouseEnter(ref:CellReference):Void {
		if (state.selection == null || state.selection.activeMouseSelection == null) return;

		// Disable drag & drop selection unless free selection is available
		var isFreeSelection = props.getSelectionMode().match(FreeSelection);
		if (!isFreeSelection) return;

		var cells = getRange(state.selection.areaSource, ref);
		if (cells == null) return;

		var selection = {
			cells: state.selection._validatedCells.concat(cells),
			_validatedCells: state.selection._validatedCells,
			_areaCells: cells,
			activeCell: ref,
			areaSource: state.selection.areaSource,
			activeMouseSelection: state.selection.activeMouseSelection,
			editing: false
		};

		setState({
			data: state.data,
			selection: selection,
			extendedData: deriveData(state.extendedData, state.data, selection)
		}, function() {
			selectionChanged(selection);
		});
	}

	function mouseUp():Void {
		if (state.selection == null || state.selection.activeMouseSelection == null) return;

		if (state.selection.activeMouseSelection.match(Ctrl(_, _))) {
			var allIncluded = true;
			for (c in state.selection._areaCells) {
				if (!state.selection._validatedCells.has(c)) {
					allIncluded = false;
					break;
				}
			}

			if (allIncluded) {
				var selection:Selection;

				if (state.selection._validatedCells.length > state.selection._areaCells.length) {
					var activeCell:CellReference = null;
					var areaSource:CellReference = null;
					switch (state.selection.activeMouseSelection) {
						case Ctrl(active, source):
							activeCell = active;
							areaSource = source;

						case _:
					}

					var cells = state.selection._validatedCells.copyWithoutValues(state.selection._areaCells);

					selection = {
						cells: cells.copy(),
						_validatedCells: cells,
						_areaCells: [],
						activeCell: activeCell == null ?  state.selection.activeCell : activeCell,
						areaSource: areaSource == null ?  state.selection.areaSource : areaSource,
						activeMouseSelection: null,
						editing: false
					};

					var hasActiveCell = selection.activeCell != null && cells.has(selection.activeCell);
					var hasAreaSource = selection.areaSource != null && cells.has(selection.areaSource);

					if (!hasActiveCell && !hasAreaSource) {
						var last = selection.last(props.colSortingAlgo, props.rowSortingAlgo);
						if (!hasActiveCell) selection.activeCell = last;
						if (!hasAreaSource) selection.areaSource = last;
					} else if (!hasActiveCell || !hasAreaSource) {
						if (hasActiveCell) selection.areaSource = selection.activeCell;
						else selection.activeCell = selection.areaSource;
					}
				} else {
					selection = {
						cells: [state.selection.activeCell],
						_validatedCells: [],
						_areaCells: [state.selection.activeCell],
						activeCell: state.selection.activeCell,
						areaSource: state.selection.activeCell,
						activeMouseSelection: null,
						editing: false
					}
				}

				return setState({
					data: state.data,
					selection: selection,
					extendedData: deriveData(state.extendedData, state.data, selection)
				}, function() {
					selectionChanged(selection);
				});
			}
		}

		var selection = state.selection.with(activeMouseSelection = null);
		setState({selection: selection}, () -> {
			selectionChanged(selection);
		});
	}

	function getRange(
		source:CellReference,
		target:CellReference
	):Null<Set<CellReference>> {
		// js.Browser.console.time('SheetContext.getRange');
		var cells:Set<CellReference> = [source];

		var sourceCol = _cols.indexOf(source.col);
		var destCol = _cols.indexOf(target.col);
		if (sourceCol == -1 || destCol == -1) return null;

		var sourceRow = _rows.indexOf(source.row);
		var destRow = _rows.indexOf(target.row);
		if (sourceRow == -1 || destRow == -1) return null;

		var itRow = sourceRow < destRow ? sourceRow...(destRow+1) : destRow...(sourceRow+1);
		for (iRow in itRow) {
			var itCol = sourceCol < destCol ? sourceCol...(destCol+1) : destCol...(sourceCol+1);

			for (iCol in itCol) {
				var cellRef = CellReference.make(_cols.get(iCol), _rows.get(iRow));
				cells.add(cellRef);
			}
		}

		// js.Browser.console.timeEnd('SheetContext.getRange');
		return cells;
	}

	static function hasData(
		data:Map<CellReference, CellData>,
		ref:CellReference
	):Bool {
		return switch (data.get(ref)) {
			case null: false;
			case d: d.value.hasData();
		};
	}

	static function deriveData(
		prev:Map<CellReference, ExtendedCellData>,
		data:Map<CellReference, CellData>,
		selection:Null<Selection>
	):Map<String, ExtendedCellData> {
		return [for (ref => d in data) ref => cachedDerive(prev, ref, d, selection)];
	}

	static inline function cachedDerive(
		prev:Map<CellReference, ExtendedCellData>,
		ref:CellReference,
		data:CellData,
		selection:Null<Selection>
	):ExtendedCellData {
		if (prev.exists(ref)) {
			var prevData = prev.get(ref);
			var newData = derive(ref, data, selection);

			if (prevData.data == data && prevData.status == newData.status)
				return prevData;

			return newData;
		}

		return derive(ref, data, selection);
	}

	static function derive(
		ref:CellReference,
		data:CellData,
		selection:Null<Selection>
	):ExtendedCellData {
		var selected = selection != null && selection.cells.has(ref);
		var active = selected && selection.activeCell == ref;

		var status:CellStatus = None;
		if (selected) status.selected = true;
		if (active) status.active = true;
		if (selected && selection.areaSource == ref) status.areaSource = true;
		if (active && selection.editing) status.editing = true;

		return {
			// ref: ref,
			data: data,
			status: status
		};
	}
}
