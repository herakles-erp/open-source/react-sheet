package react.sheet._internal;

#if macro
import haxe.macro.Expr;
import haxe.macro.Context;
using haxe.macro.TypeTools;
#end

class Helpers {
	public static function or<T>(v:Null<T>, fallback:T):T {
		return v == null ? fallback : v;
	}
}

class ObjectHelpers {
	/**
		Return a copy of a structure, replacing given fields.

		```haxe
		object.with(a = 13, b = "hi")
		// is the same as
		{a: 13, b: "hi", otherField: object.otherField}
		```
	**/
	public static macro function with<T:{}>(object:ExprOf<T>, overrides:Array<Expr>):ExprOf<T> {
		var type = Context.typeof(object);

		var fields = switch type.follow() {
			case TAnonymous(_.get().fields => fields): fields;
			case _: throw new Error("Not an anonymous structure", object.pos);
		}

		var objectDecl:Array<ObjectField> = [];
		var overriden = new Map();

		for (expr in overrides) {
			switch expr {
				case macro $i{fieldName} = $value:
					objectDecl.push({field: fieldName, expr: value});
					overriden[fieldName] = true;
				case macro $i{fieldName}:
					objectDecl.push({field: fieldName, expr: macro @:pos(expr.pos) $i{fieldName}});
					overriden[fieldName] = true;
				case {expr: EDisplay(macro null, DKMarked), pos: p}: // toplevel completion
					var remainingFieldsCT = TAnonymous([
						for (field in fields) if (!overriden.exists(field.name)) {
							pos: field.pos,
							name: field.name,
							doc: field.doc,
							kind: FVar(field.type.toComplexType())
						}
					]);
					return {pos: p, expr: EDisplay({pos: p, expr: EField(macro (null : $remainingFieldsCT), "")}, DKDot)};
				case _:
					throw new Error("Invalid override expression, should be field=value", expr.pos);
			}
		}

		for (field in fields) {
			var fieldName = field.name;
			if (!overriden.exists(fieldName)) {
				objectDecl.push({field: fieldName, expr: macro @:pos(object.pos) tmp.$fieldName});
			}
		}

		var ct = type.toComplexType();
		var expr = {expr: EObjectDecl(objectDecl), pos: Context.currentPos()};
		return macro @:pos(expr.pos) ({ var tmp = $object; $expr; } : $ct);
	}

	public static function copy<T:{}>(obj:T, ?obj2:Dynamic):T {
		#if (!macro && js)
		return js.Object.assign({}, obj, obj2);
		#else
		var target:T = cast {};
		for (field in Reflect.fields(obj))
			Reflect.setField(target, field, Reflect.field(obj, field));
		if (obj2 != null)
			for (field in Reflect.fields(obj2))
				Reflect.setField(target, field, Reflect.field(obj2, field));
		return target;
		#end
	}
}

class MapHelpers {
	public static function with<TKey, TValue>(
		map:Map<TKey, TValue>,
		key:TKey,
		value:TValue
	):Map<TKey, TValue> {
		map.set(key, value);
		return map;
	}
}

class ColorHelpers {
	public static function hexToRgb(hex:String):Col {
		if (hex == null) return null;
		if (hex.indexOf("#") == 0) hex = hex.substr(1, 6);

		return {
			r: Std.parseInt("0x" + hex.substr(0, 2)),
			g: Std.parseInt("0x" + hex.substr(2, 2)),
			b: Std.parseInt("0x" + hex.substr(4, 2)),
		};
	}

	public static function rgbToHsl(c:Col):ColHsl {
		var r = c.r / 255;
		var g = c.g / 255;
		var b = c.b / 255;
		var min = if (r <= g && r <=b ) r else if (g <= b) g else b;
		var max = if (r >= g && r >= b) r else if (g >= b) g else b;
		var delta = max - min;

		var hsl:ColHsl = {h:0., s:0., l:0.};
		hsl.l = max;

		if (delta != 0) {
			hsl.s = delta/max;
			var dr = ((max - r) / 6 + (delta / 2)) / delta;
			var dg = ((max - g) / 6 + (delta / 2)) / delta;
			var db = ((max - b) / 6 + (delta / 2)) / delta;

			if (r == max) hsl.h = db - dg;
			else if (g == max) hsl.h = 1 / 3 + dr - db;
			else if (b == max) hsl.h = 2 / 3 + dg - dr;

			if (hsl.h < 0) hsl.h++;
			if (hsl.h > 1) hsl.h--;
		}

		return hsl;
	}

	public static function hslToRgb(hsl:ColHsl):Col {
		var c:Col = {r:0, g:0, b:0};
		var r = 0.;
		var g = 0.;
		var b = 0.;

		if (hsl.s == 0) {
			c.r = c.g = c.b = Math.round(hsl.l * 255);
		} else {
			var h = hsl.h * 6;
			var i = Math.floor(h);
			var c1 = hsl.l * (1 - hsl.s);
			var c2 = hsl.l * (1 - hsl.s * (h-i));
			var c3 = hsl.l * (1 - hsl.s * (1 - (h-i)));

			switch (i) {
				case 0: r = hsl.l; g = c3; b = c1;
				case 1: r = c2; g = hsl.l; b = c1;
				case 2: r = c1; g = hsl.l; b = c3;
				case 3: r = c1; g = c2; b = hsl.l;
				case 4: r = c3; g = c1; b = hsl.l;
				case _: r = hsl.l; g = c1; b = c2;
			}

			c.r = Math.round(r * 255);
			c.g = Math.round(g * 255);
			c.b = Math.round(b * 255);
		}

		return c;
	}

	public static function rgbToInt(c:Col):Int return (c.r << 16) | (c.g << 8) | c.b;

	public static function intToHex(c:Int, ?leadingZeros = 6):String {
		var h = StringTools.hex(c);
		while (h.length < leadingZeros) h = "0" + h;
		return "#" + h;
	}

	public static function isWhite(c:String):Bool {
		var rgb = hexToRgb(c);
		// TODO: allow some small differences?
		if (rgb.r != rgb.g || rgb.r != rgb.b || rgb.g != rgb.b) return false;
		return rgb.r >= 220;
	}

	public static function brightenHex(c:String, delta:Float):String {
		return intToHex(rgbToInt(hslToRgb(brightenHsl(rgbToHsl(hexToRgb(c)), delta))));
	}

	public static function brightenHsl(hsl:ColHsl, delta:Float):ColHsl {
		if (delta < 0) {
			// Darken
			hsl.l += delta;
			if (hsl.l < 0) hsl.l = 0;
		} else {
			// Brighten
			var d = 1 - hsl.l;
			if (d > delta) hsl.l += delta;
			else {
				hsl.l = 1;
				hsl.s -= delta - d;
				if (hsl.s < 0) hsl.s = 0;
			}
		}

		return hsl;
	}
}

typedef Col = {
	var r:Int; // 0-255
	var g:Int; // 0-255
	var b:Int; // 0-255
}

typedef ColHsl = {
	var h:Float; // 0-1
	var s:Float; // 0-1
	var l:Float; // 0-1
}
