package react.sheet._internal;

import thx.csv.Dsv;

@:publicFields
class CSVUtils {
	static function isCSV(data:Array<Array<String>>):Bool {
		if (data == null) return false;
		if (data.length == 0) return false;
		if (data.length == 1 && data[0].length <= 1) return false;
		return true;
	}

	static function hasSingleData(data:Array<Array<String>>):Bool {
		if (data == null) return false;
		if (data.length == 1 && data[0].length == 1) return true;
		return false;
	}

	// Warning: assumes you checked with hasSingleData() yourself!
	static inline function getSingleData(data:Array<Array<String>>):String {
		return data[0][0];
	}

	static function decode(data:String):Array<Array<String>> {
		if (data == null) return null;
		var ret = Dsv.decode(data, {delimiter: "\t", trimEmptyLines: false});

		// Remove last line if empty (trailing \n)
		var len = ret.length;
		if (ret[len - 1].length == 0 || (ret[len - 1].length == 1 && ret[len - 1][0] == ""))
			ret = ret.slice(0, len - 2);

		return ret;
	}

	static function encode(data:Array<Array<String>>):String {
		if (data == null) return null;
		return Dsv.encode(data, {delimiter: "\t"});
	}
}
