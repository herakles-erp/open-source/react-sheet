package react.sheet._internal;

enum ClickModifier {
	None;
	Shift;
	Ctrl(?prevActive:CellReference, ?prevSource:CellReference);
}
