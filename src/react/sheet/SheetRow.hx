package react.sheet;

import react.ReactContext;
import react.sheet._internal.Context;

private typedef Props = {
	@:optional var row:String;
	@:optional var className:String;
	@:optional var children:ReactFragment;
}

@:context(SheetContext.Context)
class SheetRow extends ReactComponent<Props> {
	override function render():ReactFragment {
		var ctx:RowContextProps = Object.assign({}, context, {
			row: props.row
		});

		return
			<RowContext.Provider value={ctx}>
				<tr className={props.className}>
					{props.children}
				</tr>
			</RowContext.Provider>;
	}

	override function componentDidMount():Void {
		if (props.row != null) context.registerRow(props.row);
	}

	override function componentWillUnmount():Void {
		if (props.row != null) context.unregisterRow(props.row);
	}
}

typedef RowContextProps = {
	> SheetContextProps,
	> RowOwnContextData,
}

typedef RowContextData = {
	> SheetContextData,
	> RowOwnContextData,
}

typedef RowOwnContextData = {
	var row:String;
}

typedef RowContextProviderProps = {
	var value:RowContextProps;
}

typedef RowContextConsumerProps = {
	var children:RowContextProps->ReactFragment;
}

class RowContext {
	public static var Context(get, null):ReactContext<RowContextProps>;
	public static var Provider(get, null):ReactTypeOf<RowContextProviderProps>;
	public static var Consumer(get, null):ReactTypeOf<RowContextConsumerProps>;

	static function get_Context() {ensureReady(); return Context;}
	static function get_Provider() {ensureReady(); return Provider;}
	static function get_Consumer() {ensureReady(); return Consumer;}

	static function ensureReady() @:bypassAccessor {
		if (Context == null) {
			Context = React.createContext();
			Context.displayName = "RowContext";
			Consumer = Context.Consumer;
			Provider = Context.Provider;
		}
	}
}
