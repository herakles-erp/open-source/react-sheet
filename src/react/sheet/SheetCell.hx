package react.sheet;

import css.Properties;
import js.html.InputElement;
import js.html.KeyEvent.*;

import mui.core.InputBase;
import react.sheet.CellData;
import react.sheet.CellReference;
import react.sheet.SheetRow;
import react.sheet._internal.CellWrapper;
import react.sheet._internal.Context;
import react.types.SyntheticEvent.KeyboardEvent;
import react.types.SyntheticEvent.MouseEvent;

private typedef Props = {
	var col:String;

	// Note: currently those 3 props are only read upon component mounting,
	// updates will be ignored
	@:optional var valueKind:ValueKind;
	@:optional var valueExtractor:ValueExtractor;
	@:optional var editableValueExtractor:EditableValueExtractor;

	@:optional var getStyles:(data:CellValue)->StyleProps;
	@:optional var disabled:Bool;
	@:optional var initialValue:CellValue;
	@:optional var renderValue:Null<CellValue>->String;
	@:optional var colSpan:Int;
	@:optional var rowSpan:Int;
	@:optional var height:CSSLength;
	@:optional var width:CSSLength;
	@:optional var minWidth:CSSLength;
	@:optional var maxWidth:CSSLength;
	@:optional var title:String;
}

@:context(RowContext.Context)
class SheetCell extends ReactComponent<Props> {
	var ref:CellReference;

	function new(props:Props, context:RowContextProps) {
		super(props, context);

		ref = CellReference.make(props.col, context.row);
		register();
	}

	function register():Void {
		var data = null;
		if (props.disabled || props.initialValue != null)
			data = {ref: null, disabled: props.disabled, value: props.initialValue};

		var valueExtractor = null;
		if (props.valueExtractor != null) valueExtractor = props.valueExtractor;
		else if (props.valueKind != null)
			valueExtractor = switch (props.valueKind) {
				case IntValues: SheetUtils.intValueExtractor;
				case FloatValues: SheetUtils.floatValueExtractor;
				// case BoolValues: SheetUtils.boolValueExtractor;
				case StringValues: SheetUtils.stringValueExtractor;
			};

		context.registerCell(
			context.row,
			props.col,
			data,
			valueExtractor,
			props.editableValueExtractor,
			update
		);
	}

	var _update:ExtendedCellData->Void;
	function update(d:ExtendedCellData):Void if (_update != null) _update(d);

	override function render():ReactFragment {
		var data = context.getData(ref);

		return
			<InnerSheetCell
				cellRef={ref}
				data={data}
				context={context}
				title={props.title}
				setUpdater={cb -> _update = cb}
				{...props}
			/>;
	}

	override function componentWillUnmount():Void {
		context.unregisterCell(context.row, props.col);
	}
}

private typedef InnerProps = {
	> InnerPublicProps,
	var classes:TClasses;
}

private typedef TClasses = Classes<[
	content,
	selectedContent,
	inheritBg,
	allowOverflow
]>

private typedef InnerPublicProps = {
	> Props,

	var col:String;
	var cellRef:CellReference;
	var data:ExtendedCellData;
	var context:RowContextProps;
	var setUpdater:(cb:ExtendedCellData->Void)->Void;
	@:optional var title:String;
}

private typedef State = {
	var data:ExtendedCellData;
	var propsData:ExtendedCellData;
}

@:publicProps(InnerPublicProps)
@:wrap(Styles.withStyles(styles))
private class InnerSheetCell extends ReactComponent<InnerProps, State> {
	public static function styles(_):ClassesDef<TClasses> {
		return Styles.jss({
			content: {
				position: Relative,
				fontSize: "inherit !important",
				pointerEvents: None,
				zIndex: 1
			},
			selectedContent: {
				zIndex: 100
			},
			inheritBg: {
				background: "inherit",
			},
			allowOverflow: {
				whiteSpace: NoWrap
			}
		});
	}

	var _exited:Bool;
	var _initialValue:String;
	var ref:ReactRef<InputElement> = React.createRef();

	function new (props:InnerProps) {
		super(props);

		state = getDerivedStateFromProps(props, null);
		props.setUpdater(update);
	}

	static function getDerivedStateFromProps(props:InnerProps, prevState:State):Null<Partial<State>> {
		if (prevState == null || prevState.propsData != props.data) {
			return {
				propsData: props.data,
				data: props.data
			};
		}

		return null;
	}

	override function render():ReactFragment {
		var data = getData(state.data);
		var selected = state.data != null && state.data.status.selected;
		var displayedValue:String = (props.renderValue.or(renderValue))(data);

		return
			<CellWrapper
				onMouseUp={onMouseUp}
				onMouseDown={onMouseDown}
				onMouseEnter={onMouseEnter}
				onDoubleClick={onDoubleClick}
				colSpan={props.colSpan}
				rowSpan={props.rowSpan}
				height={props.height}
				width={props.width}
				minWidth={props.minWidth}
				maxWidth={props.maxWidth}
				getStyles={getStyles(data)}
				selected={selected}
				active={state.data != null && state.data.status.active}
				editing={state.data != null && state.data.status.editing}
				areaSource={state.data != null && state.data.status.areaSource}
				title={props.title.or(displayedValue)}
			>
				<if {state.data != null && state.data.status.editing}>
					<InputBase
						autoFocus
						className={classNames({
							'${props.classes.content}': true,
							'${props.classes.selectedContent}': selected
						})}
						defaultValue={getInitialValue(data)}
						onKeyUp={keyUp}
						onBlur={_ -> exit()}
						inputProps={{style: {padding: 0}}}
						inputRef={ref}
					/>
				<else>
					<span className={classNames({
						'${props.classes.content}': true,
						'${props.classes.selectedContent}': selected,
						'${props.classes.inheritBg}': props.context.allowOverflow, // && !selected,
						'${props.classes.allowOverflow}': props.context.allowOverflow,
					})}>
						{displayedValue}
					</span>
				</if>
			</CellWrapper>;
	}

	function renderValue(v:Null<CellValue>):String {
		if (v == null) return null;
		return switch (v.value) {
			case null | Empty: null;
			case Invalid(e): '#ERR($e)';
			case Some(v): Std.string(v);
		};
	}

	function getData(data):Null<CellValue> {
		if (data == null || data.data == null) return null;
		if (data.data.value == null) return null;
		return data.data.value;
	}

	function getStyles(data) {
		return props.getStyles == null ? null : props.getStyles.bind(data);
	}

	function getInitialValue(?data:Null<CellValue>):Null<String> {
		var selection = props.context.getSelection();

		if (selection.initialValue.or("") != "")
			return selection.initialValue;

		if (data == null) data = getData(state.data);
		if (data == null) return null;
		return props.context.extractEditableValue(data, props.cellRef);
	}

	var lastClick:Float = -1;
	function onMouseDown(e:MouseEvent<DOMElement>):Void {
		var now = js.lib.Date.now();
		var delta = now - lastClick;
		lastClick = now;

		if (delta < 1000) {
			e.preventDefault();
			e.stopPropagation();
			lastClick = -1;
			return;
		}

		var shiftKey = e.shiftKey;
		var ctrlKey = e.ctrlKey;

		props.context.cellMouseDown(props.cellRef, switch [shiftKey, ctrlKey] {
			case [true, _]: Shift;
			case [_, true]: Ctrl(null, null);
			case [false, false]: None;
		});
	}

	function onMouseUp(e:MouseEvent<DOMElement>):Void {
		e.stopPropagation();
		props.context.mouseUp();
	}

	function onMouseEnter(e:MouseEvent<DOMElement>):Void {
		props.context.cellMouseEnter(props.cellRef);
	}

	function onDoubleClick(e:MouseEvent<DOMElement>):Void {
		e.preventDefault();
		e.stopPropagation();
		props.context.startEditing();
	}

	function keyUp(e:KeyboardEvent<DOMElement>):Void {
		if (ref.current == null) return;

		switch (e.keyCode) {
			case DOM_VK_RETURN:
				e.preventDefault();
				e.stopPropagation();
				exit();

			case _:
		}
	}

	function exit():Void {
		if (_exited) return;
		_exited = true;

		var val = ref.current.value;
		if (val == "") val = null;

		if (val == null && _initialValue == "") {
			props.context.abortEditing();
			props.context.focus();
		} else {
			// TODO: compare val & initial value?
			props.context.setSelectionValue(val);
		}
	}

	function update(data:ExtendedCellData):Void {
		setState({data: data});
	}

	override function shouldComponentUpdate(nextProps:InnerProps, nextState:State) {
		return
			(state.data != nextState.data)
			|| !shallowCompare(props, nextProps)
			|| getStyles(getData(state.data)) != getStyles(getData(nextState.data));
	}

	override function componentDidUpdate(_:InnerProps, prevState:State) {
		if (state.data != null && state.data.status.editing) {
			if (prevState.data == null || !prevState.data.status.editing) {
				_exited = false;
				_initialValue = getInitialValue().or("");
			}
		}
	}

	// TODO: use a macro to make sure this stays in sync with props
	static function shallowCompare(a:InnerProps, b:InnerProps):Bool {
		if (a.col != b.col) return false;
		if (a.disabled != b.disabled) return false;
		if (a.colSpan != b.colSpan) return false;
		if (a.rowSpan != b.rowSpan) return false;
		if (a.height != b.height) return false;
		if (a.width != b.width) return false;
		if (a.minWidth != b.minWidth) return false;
		if (a.maxWidth != b.maxWidth) return false;
		if (a.title != b.title) return false;
		return true;
	}
}
