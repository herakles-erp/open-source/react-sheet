package react.sheet;

import react.sheet.CellData;

using StringTools;

@:publicFields
class SheetUtils {
	static function stringComparison(ref1:String, ref2:String):Int {
		if (ref1 == ref2) return 0;
		return ref1 < ref2 ? -1 : 1;
	}

	static function ltrimChar(str:String, char:String):String {
		var str = str;
		var len = char.length;
		while (str.startsWith(char)) str = str.substr(len);
		return str;
	}

	static function intComparison(ref1:String, ref2:String):Int {
		var r1:Int = Std.parseInt(ref1);
		var r2:Int = Std.parseInt(ref2);

		#if debug
		if (Std.string(r1) != ref1)
			throw 'Invalid reference $ref1: expecting an integer representation';

		if (Std.string(r2) != ref2)
			throw 'Invalid reference $ref2: expecting an integer representation';
		#end

		return r1 - r2;
	}

	static function floatComparison(ref1:String, ref2:String):Int {
		var r1:Float = Std.parseFloat(ref1);
		var r2:Float = Std.parseFloat(ref2);

		#if debug
		if (Std.string(r1) != ref1)
			throw 'Invalid reference $ref1: expecting a number representation';

		if (Std.string(r2) != ref2)
			throw 'Invalid reference $ref2: expecting a number representation';
		#end

		if (r1 == r2) return 0;
		return r1 < r2 ? -1 : 1;
	}

	static function intSum(values:Array<CellData>):Int {
		var sum = 0;

		for (d in values) {
			switch (d.value) {
				case null:
				case {value: Some(v)}: sum += Std.parseInt(Std.string(v));
				case _:
			}
		}

		return sum;
	}

	static function foatSum(values:Array<CellData>):Float {
		var sum = 0.0;

		for (d in values) {
			switch (d.value) {
				case null:
				case {value: Some(v)}: sum += Std.parseFloat(Std.string(v));
				case _:
			}
		}

		return sum;
	}

	// Like excel columns: A, B, C, ... Z, AA, AB, AC ... ZZ
	static function lettersComparison(ref1:String, ref2:String):Int {
		var len = ref1.length;
		if (len != ref2.length) return len - ref2.length;

		for (i in 0...len) {
			var r1 = ref1.charAt(i);
			var r2 = ref2.charAt(i);

			if (r1 == r2) continue;
			return r1 < r2 ? -1 : 1;
		}

		return 0;
	}

	static function stringValueExtractor(val:Null<String>, _:CellReference):CellValue {
		if (val == null || val == "") return {raw: null, value: Empty};
		return {raw: val, value: Some(val)};
	}

	static function intValueExtractor(val:Null<String>, _:CellReference):CellValue {
		if (val == null) return {raw: null, value: Empty};
		val = val.trim();
		if (val == "") return {raw: null, value: Empty};
		val = ltrimChar(val, "0");
		if (val == "") val = "0";

		var i = Std.parseInt(val);
		if (Std.string(i) != val) return {raw: val, value: Invalid('Not an int: $val')};
		return {raw: val, value: Some(i)};
	}

	// TODO: round to ~5th decimal to avoid X.X000000000000001 values
	static function floatValueExtractor(val:Null<String>, _:CellReference):CellValue {
		if (val == null) return {raw: null, value: Empty};
		val = val.trim();
		if (val == "") return {raw: null, value: Empty};

		var f = Std.parseFloat(val);
		if (Std.string(f) != val) return {raw: val, value: Invalid('NaN: $val')};
		return {raw: val, value: Some(f)};
	}

	static function positiveIntValueExtractor(val:Null<String>, _:CellReference):CellValue {
		if (val == null) return {raw: null, value: Empty};
		val = val.trim();
		if (val == "") return {raw: null, value: Empty};
		val = ltrimChar(val, "0");
		if (val == "") val = "0";

		var i = Std.parseInt(val);
		if (Math.isNaN(i)) return {raw: val, value: Invalid("Not a Number")};
		if (Std.string(i) != val) return {raw: val, value: Invalid("NaI")};
		if (i < 0) return {raw: val, value: Invalid("Positive value expected")};
		return {raw: val, value: Some(i)};
	}

	// TODO: round to ~5th decimal to avoid X.X000000000000001 values
	static function positiveFloatValueExtractor(val:Null<String>, _:CellReference):CellValue {
		if (val == null) return {raw: null, value: Empty};
		val = val.trim();
		if (val == "") return {raw: null, value: Empty};

		var f = Std.parseFloat(val);
		if (Math.isNaN(f)) return {raw: val, value: Invalid("Not a Number")};
		if (Std.string(f) != val) return {raw: val, value: Invalid("NaN")};
		if (f < 0) return {raw: val, value: Invalid("Positive value expected")};
		return {raw: val, value: Some(f)};
	}

	static function getRow(
		data:Map<CellReference, CellData>,
		row:String
	):Array<CellData> {
		return [for (ref => cellData in data) {
			if (ref.row != row) continue;
			cellData;
		}];
	}

	static function getCol(
		data:Map<CellReference, CellData>,
		col:String
	):Array<CellData> {
		return [for (ref => cellData in data) {
			if (ref.col != col) continue;
			cellData;
		}];
	}
}
