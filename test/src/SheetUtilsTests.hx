import buddy.SingleSuite;
import react.sheet._internal.SheetUtils;

using buddy.Should;

class SheetUtilsTests extends SingleSuite {
	public function new() {
		describe('[Sheet] Sorting algorithms', {
			it('`stringComparison` should sort strings alphabetically', {
				var arr = ["bar", "b", "foobar", "B", " foo", "foo"];
				arr.sort(SheetUtils.stringComparison);

				var expected = [" foo", "B", "b", "bar", "foo", "foobar"];
				for (i => v in expected) arr[i].should.be(v);
			});

			it('`intComparison` should sort string representation of integers', {
				var arr = ["12", "3", "1", "99", "2"];
				arr.sort(SheetUtils.intComparison);

				var expected = ["1", "2", "3", "12", "99"];
				for (i => v in expected) arr[i].should.be(v);
			});

			it('`floatComparison` should sort string representation of floats', {
				var arr = ["0.25", "0.42", "0.5", "99.99", "1.99", "1", "0.1", "2", "10"];
				arr.sort(SheetUtils.floatComparison);

				var expected = ["0.1", "0.25", "0.42", "0.5", "1", "1.99", "2", "10", "99.99"];
				for (i => v in expected) arr[i].should.be(v);
			});

			it('`lettersComparison` should sort excel column names', {
				var arr = ["B", "C", "AA", "A", "D", "E", "AB", "AC", "AAA", "ZZ", "ACC"];
				arr.sort(SheetUtils.lettersComparison);

				var expected = ["A", "B", "C", "D", "E", "AA", "AB", "AC", "ZZ", "AAA", "ACC"];
				for (i => v in expected) arr[i].should.be(v);
			});
		});
	}
}
