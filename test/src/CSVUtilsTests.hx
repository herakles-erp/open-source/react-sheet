import buddy.SingleSuite;
import react.sheet._internal.CSVUtils;

using buddy.Should;

// TODO: write more tests here. Note that some csv parsing tests are already
// written there: https://github.com/fponticelli/thx.csv/blob/master/test/TestAll.hx
class CSVUtilsTests extends SingleSuite {
	public function new() {
		describe('[Sheet] CSV utils', {
			it('decode simple data', {
				var data = '0\t1\t2\n3\t4\n\n5';
				var arr = CSVUtils.decode(data);
				arr.length.should.be(4);

				arr[0].length.should.be(3);
				arr[0].join(',').should.be('0,1,2');

				arr[1].length.should.be(2);
				arr[1].join(',').should.be('3,4');

				arr[2].length.should.be(1);
				arr[2][0].should.be("");

				arr[3].length.should.be(1);
				arr[3][0].should.be("5");
			});
		});
	}
}
