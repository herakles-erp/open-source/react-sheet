# Sheet component for haxe-react

TODO: proper README

## Sample basic use

```haxe
import react.sheet.CellData;
import react.sheet.CellReference;
import react.sheet.Sheet;
import react.sheet.SheetRow;
import react.sheet.SheetCell;
import react.sheet.SheetUtils;
import react.sheet.StaticCell;

class Test extends ReactComponent {
	override public function render() {
		return jsx(<>
			<Sheet
				valueKind={IntValues}
				colSortingAlgo={SheetUtils.stringComparison}
				rowSortingAlgo={SheetUtils.stringComparison}
				header={
					<SheetRow>
						<StaticCell key="" background="#acacac" />

						<for {j in 0...20}>
							<StaticCell
								key={String.fromCharCode("A".code + j)}
								children={String.fromCharCode("A".code + j)}
								background="#acacac"
							/>
						</for>

						<StaticCell key="sum" children="=" background="#acacac" />
					</SheetRow>
				}
			>
				<for {i in 1...21}>
					<SheetRow
						key={Std.string(pad(i))}
						row={Std.string(pad(i))}
					>
						<StaticCell
							key=""
							children={Std.string(i)}
							background="#acacac"
							align="right"
						/>

						<for {j in 0...20}>
							<SheetCell
								key={String.fromCharCode("A".code + j)}
								col={String.fromCharCode("A".code + j)}
								title={String.fromCharCode("A".code + j) + Std.string(i)}
							/>
						</for>

						<StaticCell
							key="sum"
							render={(data, row, _) -> sum(SheetUtils.getRow(data, row))}
							background="#acacac"
						/>
					</SheetRow>
				</for>

				<SheetRow key="sum" row="sum">
					<StaticCell key="" background="#acacac" children="=" />

					<for {j in 0...20}>
						<StaticCell
							key={String.fromCharCode("A".code + j)}
							col={String.fromCharCode("A".code + j)}
							render={(data, _, col) -> sum(SheetUtils.getCol(data, col))}
							background="#acacac"
						/>
					</for>

					<StaticCell key="sum" background="#acacac" />
				</SheetRow>
			</Sheet>
		</>);
	}

	static function sum(values:Array<CellData>):Int {
		var sum = 0;

		for (d in values) {
			switch (d.value) {
				case null:
				case {value: Some(v)}:
					sum += Std.parseInt(Std.string(v));
				case _:
			}
		}

		return sum;
	}


	static function pad(i:Int, ?digits:Int = 2, ?char:String = "0"):String {
		var p = [while (--digits > 0 && i < Math.pow(10, digits)) char];
		p.push(Std.string(i));
		return p.join("");
	}
}
```
